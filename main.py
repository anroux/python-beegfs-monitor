#!/usr/bin/env python3
import os
import os.path
import socket
import time

from influxdb import InfluxDBClient

def storage_directories():
    with open("/etc/beegfs/beegfs-storage.conf") as f:
        for line in f:
            line = line.strip()

            if len(line) == 0 or line.startswith("#"):
                continue

            key, value = map(lambda x:x.strip(), line.split("="))

            if key == "storeStorageDirectory":
                directories = filter(lambda x:len(x) > 0, map(lambda x:x.strip(), value.split(",")))
                return list(directories)

    raise Exception("Unable to locate eventlog socket path")

def blockdev_from_dir(directory):
    with open("/proc/mounts") as f:
        for line in f:
            blockdev, mount_dir, _ = line.split(" ", 2)
            if mount_dir == directory:
                return blockdev

def parse_stat(stat_line):
    # Name            units         description
    # ----            -----         -----------
    # read I/Os       requests      number of read I/Os processed
    # read merges     requests      number of read I/Os merged with in-queue I/O
    # read sectors    sectors       number of sectors read
    # read ticks      milliseconds  total wait time for read requests
    # write I/Os      requests      number of write I/Os processed
    # write merges    requests      number of write I/Os merged with in-queue I/O
    # write sectors   sectors       number of sectors written
    # write ticks     milliseconds  total wait time for write requests
    # in_flight       requests      number of I/Os currently in flight
    # io_ticks        milliseconds  total time this block device has been active
    # time_in_queue   milliseconds  total wait time for all requests

    (
        read_io,  read_merges,  read_sectors,  read_ticks,
        write_io, write_merges, write_sectors, write_ticks,
        in_flight, io_ticks, time_in_queue
    ) = map(int, stat_line.split())

    return {
        "read_io":       read_io,
        "read_merges":   read_merges,
        "read_sectors":  read_sectors,
        "read_ticks":    read_ticks,
        "write_io":      write_io,
        "write_merges":  write_merges,
        "write_sectors": write_sectors,
        "write_ticks":   write_ticks,
        "in_flight":     in_flight,
        "io_ticks":      io_ticks,
        "time_in_queue": time_in_queue
    }


def craft_stats(blockdevs):
    for blockdev in blockdevs:
        iostats = None
        with open(os.path.join("/sys/class/block", os.path.basename(blockdev), "stat")) as f:
            for line in f:
                iostats = parse_stat(line)
                break

        yield {
            "measurement": "iostat",
            "tags": {
                "host": socket.gethostname(),
                "disk": blockdev
            },
            "fields": iostats
        }

if __name__ == "__main__":
    blockdevs = list(map(blockdev_from_dir, storage_directories()))

    client = InfluxDBClient(host="gordrakk.esrf.fr", port=8086, database="beegfs")
    while True:
        client.write_points(craft_stats(blockdevs))
        time.sleep(1)
